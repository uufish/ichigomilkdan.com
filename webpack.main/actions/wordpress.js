import request from 'superagent'
import { states } from '../states'

function getUuid () {
  return new Date().getTime().toString(16) + Math.floor(1000 * Math.random()).toString(16)
}

const url = process.env.apiHost

class Wordpress {
  fetchPage (name, query = {}) {
    if (states['wordpress'].pages[name]) return
    const uuid = getUuid()
    states['async'].add(uuid)
    request
    .get(url + 'wp-json/wp/v2/pages')
    .query(query)
    .end((err, res) => {
      if (res) {
        const data = res.body[0]
        states['wordpress'].pages[name] = data
        states['async'].remove(uuid)
      }
    })
  }

  fetchTaxonomies (name, query = {}) {
    if (states['wordpress'].taxonomies[name]) return
    const uuid = getUuid()
    states['async'].add(uuid)
    request
    .get(url + 'wp-json/wp/v2/' + name)
    .query(query)
    .end((err, res) => {
      if (res) {
        const data = res.body;
        data.forEach(item => {
          states['wordpress'].taxonomies.ids[String(item.id)] = item
        })
        states['wordpress'].taxonomies[name] = data
        states['async'].remove(uuid)
      }
    })
  }

  fetchPosts (name, index, query) {
    if (states['wordpress'].posts[name])
      if (states['wordpress'].posts[name][index]) return
    const uuid = getUuid()
    states['async'].add(uuid)
    request
    .get(url + 'wp-json/wp/v2/' + name)
    .query(query)
    .end((err, res) => {
      if (res) {
        if (!states['wordpress'].posts[name]) {
          states['wordpress'].posts[name] = []
        }
        const dataArray = res.body
        dataArray.map(data => {
          if (data.date_gmt) {
            const dateObject = new Date(data.date_gmt)
            data.dateArray = [
              '' + dateObject.getFullYear(),
              ('0' + (dateObject.getMonth() + 1)).slice(-2),
              ('0' + dateObject.getDate()).slice(-2)
            ]
            data.monthName = [
              'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'sep', 'Oct', 'Nov', 'Dec'
            ][dateObject.getMonth()]
          }
          return data
        })
        states['wordpress'].posts[name][index] = res.body
        states['async'].remove(uuid)
      }
    })
  }

  fetchAllPosts (name, callback) {
    if (states['wordpress'].posts.index[name]) {
      if (callback) {
        callback()
      }
      return
    }
    const uuid = getUuid()
    states['async'].add(uuid)
    request
    .get(url + 'wp-json/wp/v2/' + name)
    .query({ per_page: 100 })
    .end((err, res) => {
      if (res) {
        if (!states['wordpress'].posts[name]) {
          states['wordpress'].posts[name] = []
        }
        const dataArray = res.body
        dataArray.map(data => {
          if (data.date_gmt) {
            const dateObject = new Date(data.date_gmt)
            data.dateArray = [
              '' + dateObject.getFullYear(),
              ('0' + (dateObject.getMonth() + 1)).slice(-2),
              ('0' + dateObject.getDate()).slice(-2)
            ]
            data.monthName = [
              'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'sep', 'Oct', 'Nov', 'Dec'
            ][dateObject.getMonth()]
          }
          return data
        })
        states['wordpress'].posts.index[name] = res.body
        states['async'].remove(uuid)
        if (callback) {
          callback()
        }
      }
    })
  }
}

export { Wordpress }
