import page from 'page'
import { states } from '../states'

class Router {
  go (url: string) {
    page(url)
  }

  setPage (page: string) {
    /**
     * ページを設定する
     */
    states.router.page = page
  }

  setId (id: string) {
    states.router.id = id
  }

  setQuery (query: string) {
    states.router.query = query
  }
}

export { Router }
