import { states } from '../states'

class Other {
  scrollOver (value) {
    const { other } = states
    if (value < 100) {
      if (other.scrollOver) {
        other.scrollOver = false
      }
    } else {
      if (!other.scrollOver) {
        other.scrollOver = true
      }
    }
  }

  constructor () {
    let resizeEvent = false
    window.addEventListener('resize', () => {
      if (!resizeEvent) clearTimeout(resizeEvent)
      resizeEvent = setTimeout(
        () => {
          const responsive = document.documentElement.clientWidth < 720 // 420
          if (this.isResponsive !== responsive) {
            this.isResponsive = responsive
          }
        },
        Math.floor(1000 / 60 * 10)
      )
    })
  }
}

export { Other }
