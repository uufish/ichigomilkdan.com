import { Router } from './router'
import { Other } from './other'
import { Wordpress } from './wordpress'

const actions = {
  router: new Router(),
  other: new Other(),
  wordpress: new Wordpress()
}

export { actions }
