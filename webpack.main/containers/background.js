import React, { Component } from 'react'
import { observer } from 'mobx-react'

@observer
class Background extends Component {
  render () {
    return <div className='container:background'>
      <canvas id='canvas'/>
    </div>
  }

  componentDidMount () {
    const canvas = document.getElementById('canvas')
    const ctx = canvas.getContext('2d')

    function resize () {
      canvas.width = canvas.clientWidth
      canvas.height = canvas.clientHeight
    }

    window.addEventListener('resize', resize)

    resize()

    const elements = []
    const presets = {
      o (x, y, s, dx, dy) {
        return {
          x: x,
          y: y,
          r: 12 * s,
          w: 5 * s,
          dx: dx,
          dy: dy,
          draw: function (ctx, t) {
            this.x += this.dx
            this.y += this.dy
            ctx.beginPath()
            ctx.arc(this.x + +Math.sin((50 + x + (t / 10)) / 100) * 3, this.y +
              +Math.sin((45 + x + (t / 10)) / 100) * 4, this.r, 0, 2 * Math.PI, false)
            ctx.lineWidth = this.w
            ctx.strokeStyle = 'lightcoral'
            ctx.stroke()
          }
        }
      },
      x (x, y, s, dx, dy, dr, r = 0) {
        return {
          x: x,
          y: y,
          s: 20 * s,
          w: 5 * s,
          r: r,
          dx: dx,
          dy: dy,
          dr: dr,
          draw (ctx, t) {
            this.x += this.dx
            this.y += this.dy
            this.r += this.dr
            const self = this
            function line (x, y, tx, ty, c, o = 0) {
              ctx.beginPath()
              ctx.moveTo(-o + ((self.s / 2) * x), o + ((self.s / 2) * y))
              ctx.lineTo(-o + ((self.s / 2) * tx), o + ((self.s / 2) * ty))
              ctx.lineWidth = self.w
              ctx.strokeStyle = c
              ctx.stroke()
            }
            ctx.save()
            ctx.translate(this.x + Math.sin((x + (t / 10)) / 100) * 5, this.y + Math.sin((10 + x + (t / 10)) / 100) * 2)
            ctx.rotate(this.r * Math.PI / 180)
            line(-1, -1, 1, 1, 'coral')
            line(1, -1, -1, 1, 'coral')
            ctx.restore()
          }
        }
      }
    }

    for (let x = 0; x < canvas.width; ++x) {
      for (let y = 0; y < canvas.height; ++y) {
        if (Math.round(Math.random() * 20000) == 1) {
          var s = ((Math.random() * 5) + 1) / 6
          if (Math.round(Math.random()) == 1) {
            elements.push(presets.o(x, y, s, 0, 0))
          } else {
            elements.push(presets.x(x, y, s, 0, 0, ((Math.random() * 3) - 1) / 10, (Math.random() * 360)))
          }
        }
      }
    }

    setInterval(function () {
      ctx.clearRect(0, 0, canvas.width, canvas.height)
      const time = new Date().getTime()
      for (let e in elements)
        elements[e].draw(ctx, time)
    }, 10)
  }
}

export { Background }
