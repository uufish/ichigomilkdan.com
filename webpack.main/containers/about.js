import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { states } from '../states'

@observer
class About extends Component {
  render () {
    /**
     * いちごみるく団について
     * http://ichigomilkdan.com/about
     */
    return <div className='container:about'>

      {/* メインイメージ */}
      <div className='block:about-image'>
        <img className='image:about' src='/wp-content/themes/wordpress/assets/about/about.jpg'/>
      </div>

      {/* コンテンツ */}
      <p className='text:wordpress-content wordpress-content'
        dangerouslySetInnerHTML={{ __html: this.data.content.rendered }}/>

      {/* QRコード */}
      <div className='block:qr'>
        <div className='text:qr-title'>公式LINEグループ</div>
        <img className='image:qr' src='/wp-content/themes/wordpress/assets/qr.jpg'/>
      </div>

      {/* コピーライト */}
      <div className='block:copyright'>
        <h2 className='text:copyright'>
          © 2016 ichigomilkdan.com
        </h2>
      </div>

    </div>
  }

  get data () {
    return states.wordpress.pages.about
  }
}

export { About }
