import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { states } from '../states'
@observer
class Member extends Component {
  render () {
    /**
     * メンバー
     * http://ichigomilkdan.com/member
     */
    return <div className='container:member'>

      {this.forContent()}

      {/* LINEキャスト */}
      <div className='block:cast'>
        <div className='text:cast-name'>LINEキャスト</div>
        <a className='block:twitter' href={'https://twitter.com/' + 'sakitasuo'} target='_blank'>
          <img className='image:twitter'
            src='/wp-content/themes/wordpress/assets/twitter/Twitter_Social_Icon_Blue.png'/>
          <div className='text:at'>@</div>
          <div className='text:twitter'>sakitasuo</div>
        </a>
        <a className='block:twitter' href={'https://twitter.com/' + 'cubicmash'} target='_blank'>
          <img className='image:twitter'
            src='/wp-content/themes/wordpress/assets/twitter/Twitter_Social_Icon_Blue.png'/>
          <div className='text:at'>@</div>
          <div className='text:twitter'>cubicmash</div>
        </a>
        <div className='text:description'>
          二人で仲良くラインキャストをしております(੭ु´͈ ᐜ `͈)੭ु⁾⁾<br/>
          リスインの確認を団長にとってその返信をスクリーンショットして私たちに送ってください！<br/>
          ライングループに招待します！
        </div>
      </div>

      {/* LINEキャスト */}
      <div className='block:cast'>
        <div className='text:cast-name'>イラストキャスト</div>
        <div className='text:description'>
          不定期でフリーアイコンを作成しています。<br/>
          メンバーも不定期です！またキャストさんでなくてもフリーアイコンの寄付は大歓迎です\(´ω` )/！
        </div>
      </div>

      {/* コピーライト */}
      <div className='block:copyright'>
        <h2 className='text:copyright'>
          © 2016 ichigomilkdan.com
        </h2>
      </div>

    </div>
  }

  get content () {
    return [
      states.wordpress.posts['member'][0].filter(item => item.slug === 'member-first')[0],
      states.wordpress.posts['member'][0].filter(item => item.slug === 'member-second')[0],
      ... states.wordpress.posts['member'][0].filter(item => item.slug !== 'member-first' &&
      item.slug !== 'member-second')
    ]
  }

  forContent () {
    return this.content.map(item =>
      <div className='block:member-list' key={item.id}>

        {/* 名前 */}
        <div className='block:name'>
          {item.acf.type &&
          <h3 className='text:type'>{item.acf.type}</h3>}
          <h2 className='text:name' dangerouslySetInnerHTML={{ __html: item.title.rendered }}/>
        </div>

        {/* twitter */}
        {item.acf.twitter &&
        <a className='block:twitter' href={'https://twitter.com/' + item.acf.twitter} target='_blank'>
          <img className='image:twitter'
            src='/wp-content/themes/wordpress/assets/twitter/Twitter_Social_Icon_Blue.png'/>
          <div className='text:at'>@</div>
          <div className='text:twitter' dangerouslySetInnerHTML={{ __html: item.acf.twitter }}/>
        </a>}

        {/* 自己紹介 */}
        {item.acf.description &&
        <p className='text:description wordpress-content'
          dangerouslySetInnerHTML={{ __html: item.acf.description }}/>}

      </div>)
  }
}

export { Member }
