import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { Action } from './action'
import { Background } from './background'
import { Header } from './header'
import { Content } from './content'
import { states } from '../states'

@observer
class App extends Component {
  /**
   * アプリケーションのルートになるコンテナ
   */
  render () {
    return <div className='container:app'>
      <Background/>
      <Header/>
      {this.content()}
      <Action/>
    </div>
  }

  content () {
    if (states.router.page) {
     return <Content/>
    }
  }
}

export { App }
