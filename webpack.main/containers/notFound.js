import React, { Component } from 'react'
import { observer } from 'mobx-react'

@observer
class NotFound extends Component {
  render () {

    return <div className='container-notFound'>
      <div className='block-center'>
        <h2 className='text-404'>404</h2>
        <h2 className='text-not'>Not</h2>
        <h2 className='text-found'>Found</h2>
      </div>
    </div>
  }
}

export { NotFound }
