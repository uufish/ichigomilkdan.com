import React, { Component } from 'react'
import CSSTransitionGroup from 'react-addons-css-transition-group'
import { observer } from 'mobx-react'
import { Home } from './home'
import { About } from './about'
import { Member } from './member'
import { Plaza } from './plaza'
import { Rule } from './rule'
import { Loading } from './loading'
import { states } from '../states'
import { actions } from '../actions'
import { utils } from '../utils'

@observer
class Content extends Component {
  render () {
    return <div className={`container:content ${this.isResponsive}`}>
      <CSSTransitionGroup
        className='block-page-transition'
        component='div'
        transitionName='transition-page'
        transitionEnterTimeout={500}
        transitionLeaveTimeout={500}
        transitionAppear={true}
        transitionAppearTimeout={1000}>
        {this.router()}
      </CSSTransitionGroup>
    </div>
  }

  get isResponsive () {
    if (states.other.isResponsive) {
      return 'responsive'
    } else {
      return false
    }
  }

  router () {
    if (states.async.isLoading) {
      return <Loading key='loading'/>
    }

    switch (states.router.page) {
      case 'home':
        return <Home key='home'/>
      case 'about':
        return <About key='about'/>
      case 'member':
        return <Member key='member'/>
      case 'rule':
        return <Rule key='rule'/>
      case 'plaza':
        return <Plaza key='plaza'/>
      default:
        return null
    }
  }

  componentDidMount () {
    // ↓ iOSのスクロールに対する処置
    const isSmartphone = utils.isSmartphone

    const element = document.querySelector('.block-page-transition')
    // ↓ 初回時の修正
    setInterval(() => {
      if (element.scrollTop === 0) {
        element.scrollTop = 1
      }
    }, 1000)
    const scrollEvent = () => {
      actions.other.scrollOver(element.scrollTop)
      if (isSmartphone) {
        if (element.scrollTop === 0) {
          // スクロール上端のバグ補正
          element.scrollTop = 1
        } else if (2 < element.scrollHeight - element.clientHeight &&
          element.scrollHeight - element.scrollTop - element.clientHeight === 0) {
          // スクロール下端のバグ補正
          element.scrollTop = element.scrollHeight - element.clientHeight - 1
        }
      }
    }
    try { // ↓ IE9+
      scrollEvent()
      element.addEventListener('scroll', scrollEvent, false)
    } catch (err) { // ↓ for IE8-
      scrollEvent()
      element.attachEvent('onscroll', scrollEvent)
    }
  }
}

export { Content }
