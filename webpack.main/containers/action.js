import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { states } from '../states'
import { actions } from '../actions'

@observer
class Action extends Component {
  render () {
    return <div className='container:action'>
      {this.router()}
    </div>
  }

  router () {
    switch (states.router.page) {
      case 'about':
      case 'member':
      case 'plaza':
      case 'rule':
        return <a className='block:home' href='/'>
          <img src='/wp-content/themes/wordpress/assets/material-icons/ic_clear_black_24px.svg'/>
        </a>
    }
  }
}

export { Action }
