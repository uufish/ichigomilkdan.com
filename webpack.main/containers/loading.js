import React, { Component } from 'react'
import { observer } from 'mobx-react'

@observer
class Loading extends Component {
  render () {
    return <div className='container:loading'>
      <div className='container:init'>
        <div className='block:front'></div>
        <div className='block:back'></div>
      </div>
    </div>
  }
}

export { Loading }
