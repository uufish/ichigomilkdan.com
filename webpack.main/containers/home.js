import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { states } from '../states'

@observer
class Home extends Component {
  render () {
    /**
     * トップページ
     * http://ichigomilkdan.com
     */
    return <div className='container:home'>

      {/* タイトル */}
      <a className='block:app-title' href='/plaza'>
        <h1 className='text:app-title'>
          いちごみるく団
        </h1>
        <h2 className='text:app-url'>ichigomilkdan.com</h2>
        <div className='block:circle'></div>
      </a>

      {/* QRコード */}
      {!states.other.isResponsive &&
      <div className='block:qr'>
        <div className='text:qr-title'>公式LINEグループ</div>
        <img className='image:qr' src='/wp-content/themes/wordpress/assets/qr.jpg'/>
      </div>}

      {/* Twitter */}
      <a className='text:twitter' href='https://twitter.com/riogflumpoolb' target='_blank'>
        <img className='image:twitter'
          src='/wp-content/themes/wordpress/assets/twitter/Twitter_Social_Icon_Blue.png'/>
      </a>

      <div className='block:links'>

        {/* いちごみるく団とは */}
        <a className='input:about' href='/about'>
          <div className='text:name'>いちごみるく団とは</div>
          <div className='icon:hatena'>?</div>
          <div className='block:circle'></div>
        </a>

        {/* メンバー */}
        <a className='input:member' href='/member'>
          <div className='text:name'>メンバー</div>
          <div className='icon:hatena'>▲</div>
          <div className='block:circle'></div>
        </a>

        {/* ルール */}
        <a className='input:rule' href='/rule'>
          <div className='text:name'>ルール</div>
          <div className='icon:hatena'>※</div>
          <div className='block:circle'></div>
        </a>

      </div>

      <div className='block:news'>
        {this.news.map(item =>
          <div className='block:news-item' key={item.id}>
            <div className='text:news-date'>{item.dateArray[0]}年{item.dateArray[1]}月{item.dateArray[2]}日</div>
            <div className='text:news-content' dangerouslySetInnerHTML={{ __html: item.title.rendered }}/>
          </div>)}
      </div>

      {/* コピーライト */}
      <h2 className='text:copyright'>
        © 2016 ichigomilkdan.com
      </h2>

    </div>
  }

  get news () {
    if (states.other.isResponsive) {
      return [
        states.wordpress.posts.news[0][0]
      ]
    } else {
      return [
        states.wordpress.posts.news[0][0],
        states.wordpress.posts.news[0][1]
      ]
    }
  }
}

export { Home }
