import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { states } from '../states'

@observer
class Plaza extends Component {
  render () {
    /**
     * プラザ
     * http://ichigomilkdan.com/plaza
     */
    return <div className='container:plaza'>

      <div className='block:icon-list'>
        {this.freeIcon.map((item, index) =>
          <div className='block:free-icon' key={item.id} onClick={this.onSelectImage.bind(this, index)}>
            <img className='image:free-icon' src={item.media_details.sizes.thumbnail.source_url}/>
          </div>)}
      </div>

      {this.state.index !== null &&
      <div className='block:viewer'>
        <img className='image:free-icon' src={this.freeIcon[this.state.index].media_details.sizes.full.source_url}/>
        <div className='icon:close' onClick={this.onClose.bind(this)}>{'×'}</div>
      </div>}

      {/* コピーライト */}
      <div className='block:copyright'>
        <h2 className='text:copyright'>
          © 2016 ichigomilkdan.com
        </h2>
      </div>

    </div>
  }

  state = {
    index: null
  }

  get freeIcon () {
    return states.wordpress.posts.media[0]
  }

  onSelectImage (index) {
    this.setState({ index: index })
  }

  onClose () {
    this.setState({ index: null })
  }
}

export { Plaza }
