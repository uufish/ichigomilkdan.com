import React, { Component } from 'react'
import CSSTransitionGroup from 'react-addons-css-transition-group'
import { observer } from 'mobx-react'
import { states } from '../states'

@observer
class Header extends Component {
  render () {
    return <div className={`container:header ${this.isResponsive} ${this.isHide} ${this.isMinimal}`}>
      <CSSTransitionGroup
        className='block-header-transition'
        component='div'
        transitionName='transition'
        transitionEnterTimeout={400}
        transitionLeaveTimeout={400}
        transitionAppear={true}
        transitionAppearTimeout={1000}>
        {this.router()}
      </CSSTransitionGroup>
    </div>
  }

  get isHide () {
    if (states.other.isMenu) {
      return false
    }

    switch (states.router.page) {
      case 'home':
        return 'isHide'
    }

    return false
  }

  get isMinimal () {
    switch (states.router.page) {
      case 'home':
        return false
    }

    if (states.other.scrollOver) {
      return 'isMinimal'
    }

    return false
  }

  get isResponsive () {
    if (states.other.isResponsive) {
      return 'responsive'
    } else {
      return false
    }
  }

  router () {
    let name = ''

    switch (states.router.page) {
      case 'about':
        name = 'いちごみるく団について'
        break
      case 'member':
        name = 'スタッフ'
        break
      case 'news':
        name = '新着情報'
        break
      case 'plaza':
        name = 'フリーアイコン'
        break
      case 'rule':
        name = 'ルール'
        break
    }

    if (states.other.isMenu) {
      name = 'メニュー'
    }

    return <h1 className='text-header-title' key={name}>
      {name}
    </h1>
  }
}

export { Header }
