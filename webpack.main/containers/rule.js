import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { states } from '../states'

@observer
class Rule extends Component {
  render () {
    return <div className='container:rule'>

      {/* メインイメージ */}
      <div className='block:main-image'>
        <img className='image:main' src='/wp-content/themes/wordpress/assets/rule/procedure.jpg'/>
      </div>

      {/* 手続き */}
      <div className='block:rule-title'>
        <h2 className='text:rule-title'
          dangerouslySetInnerHTML={{ __html: this.data.procedure.title.rendered }}/>
      </div>

      {/* 手続き コンテンツ */}
      <p className='text:wordpress-content'
        dangerouslySetInnerHTML={{ __html: this.data.procedure.content.rendered }}/>

      {/* メインイメージ */}
      <div className='block:main-image'>
        <img className='image:main' src='/wp-content/themes/wordpress/assets/rule/caution.jpg'/>
      </div>

      {/* 注意事項 */}
      <div className='block:rule-title'>
        <h2 className='text:rule-title'
          dangerouslySetInnerHTML={{ __html: this.data.caution.title.rendered }}/>
      </div>

      {/* 注意事項 コンテンツ */}
      <p className='text:wordpress-content'
        dangerouslySetInnerHTML={{ __html: this.data.caution.content.rendered }}/>

      {/* QRコード */}
      <div className='block:qr'>
        <div className='text:qr-title'>公式LINEグループ</div>
        <img className='image:qr' src='/wp-content/themes/wordpress/assets/qr.jpg'/>
      </div>

      {/* コピーライト */}
      <div className='block:copyright'>
        <h2 className='text:copyright'>
          © 2016 ichigomilkdan.com
        </h2>
      </div>

    </div>
  }

  get data () {
    return {
      procedure: states.wordpress.pages.ruleProcedure,
      caution: states.wordpress.pages.ruleCaution
    }
  }
}

export { Rule }
