import { Async } from './async'
import { Router } from './router'
import { Other } from './other'
import { Wordpress } from './wordpress'

const states = {
  async: new Async(),
  router: new Router(),
  other: new Other(),
  wordpress: new Wordpress()
}

export { states }
