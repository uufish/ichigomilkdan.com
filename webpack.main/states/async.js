import { observable } from 'mobx'

class Async {
  /**
   * 非同期処理
   */
  @observable uuid: array = [] // uuid

  add (uuid: string) {
    /**
     * uuidを追加する
     */
    this.uuid.push(uuid)
  }

  remove (uuid: string) {
    /**
     * uuidを削除する
     */
    const index = this.uuid.indexOf(uuid)
    this.uuid.splice(index, 1)
  }

  get isLoading () {
    /**
     * uuidの数が0でない時はローディングページに移行する
     */
    return this.uuid.length !== 0
  }
}

export { Async }
