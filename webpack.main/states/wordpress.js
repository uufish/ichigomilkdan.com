import { observable } from 'mobx'

class Wordpress {
  /**
   * wordpress
   */
  @observable pages = { index: [], ids: {} } // 固定ページ

  @observable taxonomies = { index: [], ids: {} } // タクソノミ

  @observable posts = { index: [], ids: {} } // ポスト
}

export { Wordpress }
