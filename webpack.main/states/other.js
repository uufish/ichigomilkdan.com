import { observable } from 'mobx'

class Other {
  /**
   * その他
   */
  @observable isResponsive = document.documentElement.clientWidth < 720 // 420

  @observable scrollOver = false
}

export { Other }
