import { observable } from 'mobx'

class Router {
  /**
   * ルーター
   */
  @observable page = null

  @observable id = null

  @observable isOpening = true
}

export { Router }
