import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import page from 'page'
import { App } from './containers/app'
import { actions } from './actions'
import './styles/main'

function init () {
  render(
    <AppContainer>
      <App/>
    </AppContainer>,
    document.querySelector('.root\\:app')
  )
}

window.onload = function () {
  const app = document.querySelector('.root\\:app')
  if (app) {
    init()
    const attachFastClick = require('fastclick')
    attachFastClick.attach(document.body)
  }
}

if (module.hot) {
  const app = document.querySelector('.root\\:app')
  if (app) {
    module.hot.accept('./containers/app', () => {
      require('./containers/app').default
      init()
    })
  }
}

const documentTitle = document.title

page('/', ({ path }) => {
  actions['wordpress'].fetchPosts('news', 0, { per_page: 100 })
  actions['router'].setPage('home')
  document.title = documentTitle
  if (process.env.NODE_ENV === 'production') {
    window.ga('send', 'pageview', {
      'page': path,
      'title': document.title
    })
  }
})

page('/about', ({ path }) => {
  actions['wordpress'].fetchPage('about', { slug: 'about' })
  actions['router'].setPage('about')
  document.title = 'いちごみるく団とは？' + ' | ' + documentTitle
  if (process.env.NODE_ENV === 'production') {
    window.ga('send', 'pageview', {
      'page': path,
      'title': document.title
    })
  }
})

page('/member', ({ path }) => {
  actions['wordpress'].fetchPosts('member', 0, { per_page: 100 })
  actions['router'].setPage('member')
  document.title = 'スタッフ' + ' | ' + documentTitle
  if (process.env.NODE_ENV === 'production') {
    window.ga('send', 'pageview', {
      'page': path,
      'title': document.title
    })
  }
})

page('/plaza', ({ path }) => {
  actions['wordpress'].fetchPosts('media', 0, { freeIcon: true, per_page: 100 })
  actions['router'].setPage('plaza')
  document.title = documentTitle
  if (process.env.NODE_ENV === 'production') {
    window.ga('send', 'pageview', {
      'page': path,
      'title': document.title
    })
  }
})

page('/rule', ({ path }) => {
  actions['wordpress'].fetchPage('ruleProcedure', { slug: 'rule-procedure' })
  actions['wordpress'].fetchPage('ruleCaution', { slug: 'rule-caution' })
  actions['router'].setPage('rule')
  document.title = 'ルール' + ' | ' + documentTitle
  if (process.env.NODE_ENV === 'production') {
    window.ga('send', 'pageview', {
      'page': path,
      'title': document.title
    })
  }
})

page()
