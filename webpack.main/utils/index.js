import { isSmartphone } from './isSmartphone'

const utils = {
  isSmartphone
}

export { utils }
