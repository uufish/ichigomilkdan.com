const { resolve } = require('path')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')

const webpackConfig = require(resolve('webpack.config'))
const compiler = webpack(webpackConfig)

module.exports = {
	ui: {
		port: 3001,
		weinre: {
			port: 8080
		}
	},
	files: [
		resolve('wordpress/*'),
		resolve('wordpress/*/*'),
		resolve('wordpress/*/*/*')
	],
	watchOptions: {},
	server: false,
	proxy: process.env.PROXY || 'localhost',
	port: 4040,
	middleware: [
		webpackDevMiddleware(compiler, {
			publicPath: webpackConfig.output.publicPath,
			proxy: {
				'*': process.env.PROXY || 'localhost'
			}
		}), webpackHotMiddleware(compiler)
	],
	serveStatic: [],
	ghostMode: {
		clicks: false,
		scroll: false,
		forms: {
			submit: false,
			inputs: false,
			toggles: false
		}
	},
	logLevel: 'info',
	logPrefix: 'BS',
	logConnections: false,
	logFileChanges: true,
	logSnippet: true,
	rewriteRules: false,
	open: 'local',
	browser: 'default',
	xip: false,
	hostnameSuffix: false,
	reloadOnRestart: false,
	notify: true,
	scrollProportionally: true,
	scrollThrottle: 0,
	scrollRestoreTechnique: 'window.name',
	scrollElements: [],
	scrollElementMapping: [],
	reloadDelay: 0,
	reloadDebounce: 0,
	plugins: [],
	injectChanges: true,
	startPath: null,
	minify: false,
	host: null,
	codeSync: true,
	timestamps: true,
	clientEvents: ['scroll', 'scroll:element', 'input:text', 'input:toggles', 'form:submit', 'form:reset', 'click'],
	socket: {
		socketIoOptions: {
			log: false
		},
		socketIoClientConfig: {
			reconnectionAttempts: 50
		},
		path: '/browser-sync/socket.io',
		clientPath: '/browser-sync',
		namespace: '/browser-sync',
		clients: {
			heartbeatTimeout: 5000
		}
	},
	tagNames: {
		less: 'link',
		scss: 'link',
		css: 'link',
		jpg: 'img',
		jpeg: 'img',
		png: 'img',
		svg: 'img',
		gif: 'img',
		js: 'script'
	}
}
