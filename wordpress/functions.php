<?php

# ↓ custom-post-type
locate_template('functions/custom-post-type/news.php', true);
locate_template('functions/custom-post-type/member.php', true);

# ↓ custom-taxonomy
locate_template('functions/custom-taxonomy/book.php', true);

# ↓ admin
locate_template('functions/admin/admin-enqueue.php', true);
locate_template('functions/admin/admin-bar.php', true);
locate_template('functions/admin/admin-menu.php', true);
locate_template('functions/admin/manual.php', true);

# ↓ 日本語のスラッグは修正
add_filter('wp_unique_post_slug', function ($slug, $post_ID, $post_status, $post_type) {
  if (preg_match('/(%[0-9a-f]{2})+/', $slug)) {
    $slug = utf8_uri_encode($post_type).'-'.$post_ID;
  }
  return $slug;
}, 10, 4);

# ↓ エラーログの非表示
error_reporting(0);
