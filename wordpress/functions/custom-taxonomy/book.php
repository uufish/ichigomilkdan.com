<?php

# ↓ book
add_action('init', function () {
	register_taxonomy('book-taxonomy', [
		'book'
	], [
		'show_in_rest' => true,
		'hierarchical' => true,
		'label' => '本の種類',
		'singular_label' => '本の種類',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true
	]);
});
