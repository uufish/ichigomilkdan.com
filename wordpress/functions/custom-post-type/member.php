<?php

# ↓ メンバー
add_action('init', function () {
	register_post_type('member', [
		'show_in_rest' => true,
		'rest_base' => 'member',
		'labels' => [
			'name' => 'メンバー',
			'singular_name' => 'メンバー',
			'add_new' => '新しく追加',
			'add_new_item' => 'データを新しく追加',
			'edit_item' => 'データを編集する',
			'new_item' => '新規データ',
			'all_items' => '全てのデータ',
			'view_item' => 'データの説明を見る',
			'search_items' => '検索する',
			'not_found' => 'データが見つかりませんでした。',
			'not_found_in_trash' => 'ゴミ箱内にお知らせが見つかりませんでした。'
		],
		'menu_position' => 3,
		'public' => true,
		'query_var' => true,
		'has_archive' => true,
		'supports' => [
			'title'
		],
		'rewrite' => [
			'slug' => 'member',
			'with_front' => false
		],
		'taxonomies' => ['websites_category', 'websites_tag']
	]);
});
