<?php

# ↓ お知らせ
add_action('init', function () {
	register_post_type('news', [
		'show_in_rest' => true,
		'rest_base' => 'news',
		'labels' => [
			'name' => 'お知らせ',
			'singular_name' => 'お知らせ',
			'add_new' => '新しく追加',
			'add_new_item' => 'お知らせを新しく追加',
			'edit_item' => 'お知らせを編集する',
			'new_item' => '新規お知らせ',
			'all_items' => '全てのお知らせ',
			'view_item' => 'お知らせの説明を見る',
			'search_items' => '検索する',
			'not_found' => 'お知らせが見つかりませんでした。',
			'not_found_in_trash' => 'ゴミ箱内にお知らせが見つかりませんでした。'
		],
		'menu_position' => 3,
		'public' => true,
		'query_var' => true,
		'has_archive' => true,
		'supports' => [
			'title',
			'editor',
		],
		'rewrite' => [
			'slug' => 'news',
			'with_front' => false
		],
		'taxonomies' => ['websites_category', 'websites_tag']
	]);
});
