<?php

// add_filter('show_admin_bar', '__return_false');

add_action('admin_bar_menu', function ($wp_admin_bar) {
	# $wp_admin_bar->remove_menu('wp-logo'); # WordPressシンボルマーク
	# $wp_admin_bar->remove_menu('my-account'); # マイアカウント
	$wp_admin_bar->remove_menu('appearance'); # 外観
	$wp_admin_bar->remove_menu('customize'); # カスタマイズ
	# $wp_admin_bar->remove_menu('updates'); # 更新
	$wp_admin_bar->remove_menu('new-content'); # 新規
	$wp_admin_bar->remove_menu('search'); # 検索
	$wp_admin_bar->remove_menu('comments'); # コメント
}, 1000);

add_action('wp_before_admin_bar_render', function () {
	global $wp_admin_bar;
	$wp_admin_bar->add_menu([
		'id' => 'new_item_in_admin_bar',
		'title' => __('ログアウト'),
		'href' => wp_logout_url()
	]);
});
