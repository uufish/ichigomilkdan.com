<?php function page() { ?>
	<script src='<?= get_template_directory_uri() ?>/scripts/manual.js'></script>
<?php }

add_action('admin_menu', function () {
	add_menu_page('マニュアル', 'マニュアル', 'editor', 'manual', 'page');
});
