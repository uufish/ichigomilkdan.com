<?php

add_action('admin_menu', function () {
	global $menu;
	# remove_menu_page('index.php'); # ダッシュボード
	remove_menu_page('separator1'); # メニューの線1
	remove_menu_page('edit.php'); # 投稿
	# remove_menu_page('upload.php'); # メディア
	# remove_menu_page('link-manager.php'); # リンク
	# remove_menu_page('edit.php?post_type=page'); # ページ
	remove_menu_page('edit-comments.php'); # コメント
	
	remove_menu_page('separator2'); # メニューの線2
	
	remove_menu_page('themes.php'); # 外観
	remove_submenu_page('themes.php', 'themes.php'); # 外観 -> テーマ
	remove_submenu_page('themes.php', 'widgets.php'); # 外観 -> ウィジェット
	remove_submenu_page('themes.php', 'theme-editor.php'); # 外観 -> テーマ編集
	if (!current_user_can('administrator')):
		remove_menu_page('themes.php');
	endif;
	# remove_menu_page('plugins.php'); # プラグイン
	if (!current_user_can('administrator')):
		remove_menu_page('plugins.php');
	endif;
	# remove_menu_page('users.php'); # ユーザ
	# remove_menu_page('tools.php'); # ツール
	if (!current_user_can('administrator')):
		remove_menu_page('tools.php');
	endif;
	# remove_menu_page('options-general.php'); # 設定
	# remove_menu_page('profile.php'); # プロフィール
	
	unset($menu[90]); # メニューの線3
	
	# remove_meta_box('postcustom', 'post', 'normal'); # カスタムフィールド
	# remove_meta_box('postexcerpt', 'post', 'normal'); # 抜粋
	# remove_meta_box('commentstatusdiv', 'post', 'normal'); # コメント設定
	# remove_meta_box('trackbacksdiv', 'post', 'normal'); # トラックバック設定
	# remove_meta_box('revisionsdiv', 'post', 'normal'); # リビジョン表示
	# remove_meta_box('formatdiv', 'post', 'normal'); # フォーマット設定
	# remove_meta_box('slugdiv', 'post', 'normal'); # スラッグ設定
	# remove_meta_box('authordiv', 'post', 'normal'); # 投稿者
	# remove_meta_box('categorydiv', 'post', 'normal'); # カテゴリー
	# remove_meta_box('tagsdiv-post_tag', 'post', 'normal'); # タグ
});
