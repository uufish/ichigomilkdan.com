<?php

add_action('admin_head', function () {
	add_editor_style('style-editor.css');
});

add_action('admin_enqueue_scripts', function () {
	wp_enqueue_script('admin-script', get_template_directory_uri().'/scripts/admin.js');
});
