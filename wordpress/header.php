<!doctype html>

<head>
  <title><?= get_bloginfo('name') ?></title>
  <meta charset='utf-8'>
  <meta name='description' content='<?= get_bloginfo("description") ?>'>
  <meta name='viewport' content='width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no'>
  <meta name='format-detection' content='telephone=no'>
  <meta name='apple-mobile-web-app-capable' content='yes'>
  <meta name='apple-mobile-web-app-status-bar-style' content='black-translucent'>
  <link href='<?= get_template_directory_uri() ?>/style.css' rel='stylesheet'>
  <!--[if IE]>
  <script src='//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.5.7/es5-shim.min.js'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.5.7/es5-sham.min.js'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/json3/3.3.2/json3.min.js'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/es6-shim/0.34.2/es6-shim.min.js'></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/es6-shim/0.34.2/es6-sham.min.js'></script>
  <script src='https://wzrd.in/standalone/es7-shim@latest'></script>
  <![endif]-->
  <!--[if lt IE 9]>
  <script src='//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js'></script>
  <![endif]-->
  <script>
    (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-80498311-7', 'auto');
  </script>
  <script src='<?= get_template_directory_uri() ?>/scripts/main.js?0.1.4'></script>
</head>
