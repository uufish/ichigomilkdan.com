const { resolve } = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')

module.exports = {
  entry: {
    main: process.env.NODE_ENV === 'production'
      ? [
      resolve('webpack.main/main')
    ] : [
      'react-hot-loader/patch',
      'webpack-hot-middleware/client',
      resolve('webpack.main/main')
    ]
  },
  output: {
    path: resolve('wordpress/scripts'),
    publicPath: '/wp-content/themes/wordpress/scripts',
    filename: '[name].js'
  },
  module: {
    loaders: [{
      test: new RegExp('.(js|jsx)$'),
      exclude: new RegExp('node_modules'),
      loaders: ['babel-loader']
    }, {
      test: new RegExp('.styl$'),
      loaders: [
        'style-loader',
        'css-loader?camelCase&localIdentName=[local]_[name]',
        'postcss-loader', 'stylus-loader'
      ]
    }, {
      test: new RegExp('.md$'),
      loaders: ['raw-loader']
    }]
  },
  postcss() {
    return {
      plugins: [autoprefixer]
    }
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.styl', '.md'],
    root: [resolve()]
  },
  plugins: process.env.NODE_ENV === 'production' ? [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      mangle: true,
      output: { comments: require('uglify-save-license') },
      compress: {
        warnings: false,
        sequences: true,
        properties: true,
        dead_code: true,
        conditionals: true,
        booleans: true,
        unused: true,
        if_return: true,
        join_vars: true,
        drop_console: true
      },
      beautify: false,
      sourceMap: false
    }), new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.apiHost': JSON.stringify('http://ichigomilkdan.com/')
    })
  ] : [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.apiHost': JSON.stringify('http://ichigomilkdan.com/')
    })
  ],
  debug: false
}
